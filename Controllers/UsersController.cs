﻿using Kanoon.Entities;
using Kanoon.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Kanoon.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _userRepository;
        public UsersController(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet("{nationalCode}")]
        public async Task<IActionResult> Get(string nationalCode)
        {
            if (!long.TryParse(nationalCode, out var id))
                return BadRequest("کد ملی نامعتبر است");

            var user = await _userRepository.Find(id);
            if (user is null)
                return BadRequest(new
                {
                    Message = "اطلاعات دسترسی کاربر یافت نشد"
                });

            return Ok(user);
        }
    }
}
