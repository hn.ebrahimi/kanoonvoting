﻿using Kanoon.Dto;
using Kanoon.Entities;
using Kanoon.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Kanoon.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VotersController : ControllerBase
    {
        private readonly IRepository<Voter> _voterRepository;

        public VotersController(IRepository<Voter> voterRepository)
        {
            _voterRepository = voterRepository;
        }

        [HttpGet("{nationalCode}")]
        public async Task<IActionResult> Get(string nationalCode)
        {
            if (!long.TryParse(nationalCode, out var id))
                return BadRequest("کد ملی نامعتبر است");

            var voter = await _voterRepository.Find(id);

            if (voter == null) return BadRequest(new { Message = "بازنشسته یافت نشد" });

            return Ok(new
            {
                NationalCode = voter.NationalCode.ToString().PadLeft(10, '0'),
                voter.Mostameri,
                voter.FullName,
                voter.Voted,
                voter.Branch,
                VotingTime = voter.Voted ? $"{ voter.UpdateDate.Value.Hour}:{ voter.UpdateDate.Value.Minute}:{ voter.UpdateDate.Value.Second}" : string.Empty
            });
        }

        [HttpPost]
        public async Task<IActionResult> Insert(VoterPostDto voterDto)
        {
            if (!long.TryParse(voterDto.NationalCode, out var nationalCode))
                return BadRequest("کد ملی نامعتبر است");

            var voter = await _voterRepository.Find(nationalCode);
            if (voter is not null) return BadRequest(new
            {
                NationalCode = voter.NationalCode.ToString().PadLeft(10, '0'),
                voter.Mostameri,
                voter.FullName,
                voter.Voted,
                voter.Branch,
                VotingTime = voter.Voted ? $"{ voter.UpdateDate.Value.Hour}:{ voter.UpdateDate.Value.Minute}:{ voter.UpdateDate.Value.Second}" : string.Empty
            });

            var newVoter = (Voter)voterDto;
            await _voterRepository.Insert(newVoter);
            await _voterRepository.SaveChange();

            return Ok();
        }

        [HttpPut("{nationalCode}")]
        public async Task<IActionResult> Update(string nationalCode, VoterPutDto voterDto)
        {
            if (!long.TryParse(nationalCode, out var id))
                return BadRequest("کد ملی نامعتبر است");

            var voter = await _voterRepository.Find(id);
            if (voter.Voted) return BadRequest(new
            {
                voter.Voted,
                VotingTime = $"{ voter.UpdateDate.Value.Hour}:{ voter.UpdateDate.Value.Minute}:{ voter.UpdateDate.Value.Second}",
                Message = "قبلا رای ثبت شده است"
            });

            voter.Voted = true;
            voter.UpdateDate = DateTime.Now;
            voter.UserId = voterDto.UserId;
            _voterRepository.Update(voter);
            await _voterRepository.SaveChange();

            return Ok();
        }
    }
}