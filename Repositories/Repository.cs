﻿using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly KanoonContext _context;
        public Repository(KanoonContext context)
        {
            _context = context;
        }

        public async Task<TEntity> Find(object key)
        {
            var entity = await _context.Set<TEntity>().FindAsync(key);
            return entity;
        }

        public IQueryable<TEntity> Get()
        {
            return _context.Set<TEntity>().AsQueryable();
        }

        public void Update(TEntity entity)
        {
            _context.Set<TEntity>().Update(entity);
        }

        public async Task Insert(TEntity entity)
        {
           await _context.Set<TEntity>().AddAsync(entity);
        }

        public async Task SaveChange()
        {
            await _context.SaveChangesAsync();
        }
    }
}
