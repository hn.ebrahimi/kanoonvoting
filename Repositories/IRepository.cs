﻿using System.Linq;
using System.Threading.Tasks;

namespace Kanoon.Repositories
{
    public interface IRepository<TEntity>
    {
        Task<TEntity> Find(object key);
        IQueryable<TEntity> Get();
        void Update(TEntity entity);
        Task Insert(TEntity entity);
        Task SaveChange();
    }
}