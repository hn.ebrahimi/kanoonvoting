﻿using Kanoon.Entities;
using Microsoft.EntityFrameworkCore;

namespace Kanoon.Repositories
{
  public class KanoonContext : DbContext
    {
        public KanoonContext(DbContextOptions<KanoonContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Voter> Voters { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new UserMap(modelBuilder.Entity<User>());
            new VoterMap(modelBuilder.Entity<Voter>());
        }
    }
}
