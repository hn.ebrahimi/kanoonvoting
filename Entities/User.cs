﻿namespace Kanoon.Entities
{
    public class User
    {
        public string FullName { get; set; }
        public long NationalCode { get; set; }
    }
}