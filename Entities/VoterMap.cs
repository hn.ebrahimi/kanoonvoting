﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kanoon.Entities
{
    public class VoterMap
    {
        public VoterMap(EntityTypeBuilder<Voter> entityBuilder)
        {
            entityBuilder.HasKey(t => t.NationalCode);

            entityBuilder.Property(t => t.FullName).IsRequired();
            entityBuilder.Property(t => t.InsertDate).IsRequired().HasDefaultValue(DateTime.Now);
            entityBuilder.Property(t => t.Branch).IsRequired();
            entityBuilder.Property(t => t.Mostameri).IsRequired();
            entityBuilder.Property(t => t.NationalCode).IsRequired();
            entityBuilder.Property(t => t.UpdateDate).HasDefaultValue(DateTime.Now);
            entityBuilder.Property(t => t.UserId);
            entityBuilder.Property(t => t.Voted).IsRequired().HasDefaultValue(false);
        }
    }
}