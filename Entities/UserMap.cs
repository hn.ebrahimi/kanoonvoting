﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Kanoon.Entities
{
    public class UserMap
    {
        public UserMap(EntityTypeBuilder<User> entityBuilder)
        {
            entityBuilder.HasKey(t => t.NationalCode);

            entityBuilder.Property(t => t.FullName).IsRequired();
            entityBuilder.Property(t => t.NationalCode).IsRequired();
        }
    }
}