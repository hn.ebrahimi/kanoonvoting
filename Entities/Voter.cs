﻿using System;

namespace Kanoon.Entities
{
    public class Voter
    {
        public long NationalCode { get; set; }
        public string Mostameri { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
        public DateTime InsertDate { get; set; }
        public bool Voted { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Branch { get; set; }
    }
}
