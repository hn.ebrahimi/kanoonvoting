﻿using System.ComponentModel.DataAnnotations;

namespace Kanoon.Dto
{
    public class VoterPutDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "کد کاربر ثبت کننده نامعتبر است")]
        public string UserId { get; set; }
    }
}
