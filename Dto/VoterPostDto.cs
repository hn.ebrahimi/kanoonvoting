﻿using Kanoon.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace Kanoon.Dto
{
    public class VoterPostDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "کد کاربر ثبت کننده نامعتبر است")]
        public string UserId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "کد ملی بازنشسته نامعتبر است")]
        public string NationalCode { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "کد مستمری بازنشسته نامعتبر است")]
        public string Mostameri { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "نام بازنشسته نامعتبر است")]
        public string Name { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "نام خانوادگی بازنشسته نامعتبر است")]
        public string Family { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "شعبه تامین اجتماعی نامعتبر است")]
        public string Branch { get; set; }

        public static explicit operator Voter(VoterPostDto voter)
        {
            return (voter is null)
                ? null
                : new Voter
                {
                    Voted = true,
                    UpdateDate = DateTime.Now,
                    FullName = voter.Name + " " + voter.Family,
                    InsertDate = DateTime.Now,
                    Mostameri = voter.Mostameri,
                    NationalCode = long.Parse(voter.NationalCode),
                    UserId = voter.UserId,
                    Branch = voter.Branch
                };
        }
    }
}